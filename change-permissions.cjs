const fs=require("fs");

function changePermission(){
    const file="./reader.txt";
    return new Promise((resolve,reject)=>{
        fs.chmod(file,0o400,(error)=>{
            if(error){
                reject(error.message);
            }
            else{
                resolve("File permissions changed successfully");
            }
        });
    });
}

changePermission().then((response)=>{
    console.log(response);
})
.catch((error)=>console.log(error))