const fs=require("fs");
const { v4:uuid } = require('uuid');

function writeUUID(){
    let fileName="./uuid.txt";
    let currentUUID=uuid();
    return new Promise((resolve,reject)=>{
        fs.writeFile(fileName,currentUUID,(error)=>{
            if(error){
                reject(error.message);
            }
            else{
                resolve("File successfully written");
            }
        })
    })
}
function readFile(){
    let dirName="./uuid.txt";
    return new Promise((resolve, reject) =>{
        fs.readFile(dirName,'utf8',(error,data)=>{
            if(error){
                reject(error.message);
            }
            else{
                resolve(data.toUpperCase());
            }
        })
    });
}
function writeFile(data){
    let dirName="./uuid.txt";
    return new Promise((resolve, reject) =>{
        fs.writeFile(dirName,data,(error)=>{
            if(error){
                reject(error.message);
            }
            else{
                resolve("File overwritten successfully");
            }
        })
    });
}

writeUUID().then((response)=>readFile()).then((finalResponse)=>writeFile(finalResponse));