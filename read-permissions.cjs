const fs=require("fs");

function readPermissions(){
    let file="./reader.txt";
    return new Promise((resolve, reject) =>{
        fs.access(file,fs.constants.W_OK|fs.constants.R_OK|fs.constants.X_OK,(error)=>{
            if(error){
                reject("File does not have all the permissions");
            }
            else{
                resolve("File has all permissions");
            }
        })
    })
}

readPermissions().then((response)=>console.log(response))
.catch((error)=>console.log(error));