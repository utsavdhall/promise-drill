const fs=require("fs");
function readFile(){
    let dirName="./reader.txt";
    return new Promise((resolve, reject) =>{
        fs.readFile(dirName,'utf8',(error,data)=>{
            if(error){
                reject(error.message);
            }
            else{
                resolve(data);
            }
        })
    });
}

readFile().then((response)=>{
    return console.log(response);
})
.catch((error)=>{
    return console.log(error);
})