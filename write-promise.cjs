const fs=require("fs");
function writeFile(){
    let dirName="./writer.txt";
    return new Promise((resolve, reject) =>{
        fs.writeFile(dirName,"hello",(error)=>{
            if(error){
                reject(error.message);
            }
            else{
                resolve("File overwritten successfully");
            }
        })
    });
}

writeFile().then((response)=>{
    return console.log(response);
})
.catch((error)=>{
    return console.log(error);
})